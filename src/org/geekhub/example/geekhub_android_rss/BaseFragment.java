package org.geekhub.example.geekhub_android_rss;

import android.os.Bundle;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import org.geekhub.example.geekhub_android_rss.R;
import org.geekhub.example.geekhub_android_rss.util.Intents;
import com.facebook.Session;
import com.facebook.SessionState;
import org.holoeverywhere.app.Fragment;

public class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        Session session = Session.getActiveSession();
        if (session == null) {
            if (savedInstanceState != null) {
                session = Session.restoreSession(getSupportActivity(), null, null, savedInstanceState);
            }
            if (session == null) {
                session = new Session.Builder(getSupportActivity()).setApplicationId(getSupportActivity().getString(R.string.app_id)).build();
            }
            Session.setActiveSession(session);
            if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
                session.openForRead(new Session.OpenRequest(this).setCallback(null));
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                startActivity(Intents.getSettingsIntent(getActivity()));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
