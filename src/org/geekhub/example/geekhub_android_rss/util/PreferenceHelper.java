package org.geekhub.example.geekhub_android_rss.util;

import android.content.Context;
import org.holoeverywhere.preference.PreferenceManager;
import org.holoeverywhere.preference.SharedPreferences;

public class PreferenceHelper {

    private static final String PREF_KEY_TWITTER_USERNAME = "twitter_username";
    private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loged_in";

    private static SharedPreferences mSharedPreferences;

    public static void init(Context applicationContext) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);
    }

    public static void setTwitterConncetion(String username, String token, String tokenSecret) {
        if ((token == null || token.equals("")) || (tokenSecret == null || tokenSecret.equals(""))) return;

        SharedPreferences.Editor e = mSharedPreferences.edit();
        e.putString(PREF_KEY_TWITTER_USERNAME, username);
        e.putString(PREF_KEY_OAUTH_TOKEN, token);
        e.putString(PREF_KEY_OAUTH_SECRET, tokenSecret);
        e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
        e.commit();
    }

    public static boolean isTwitterConnected() {
        return mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
    }

    public static void removeTwitterConnection() {
        SharedPreferences.Editor e = mSharedPreferences.edit();
        e.remove(PREF_KEY_TWITTER_USERNAME);
        e.remove(PREF_KEY_OAUTH_TOKEN);
        e.remove(PREF_KEY_OAUTH_SECRET);
        e.remove(PREF_KEY_TWITTER_LOGIN);
        e.commit();
    }

    public static String getTwitterUsername() {
        return mSharedPreferences.getString(PREF_KEY_TWITTER_USERNAME, null);
    }

    public static String getTwitterToken() {
        return mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, null);
    }

    public static String getTwitterTokenSecret() {
        return mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, null);
    }
}
