package org.geekhub.example.geekhub_android_rss.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import org.geekhub.example.geekhub_android_rss.BaseActivity;
import org.geekhub.example.geekhub_android_rss.R;
import org.geekhub.example.geekhub_android_rss.fragments.DetailsFragment;
import org.geekhub.example.geekhub_android_rss.util.Intents;

public class DetailsActivity extends BaseActivity {

    private String mTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);

        if (savedInstanceState == null) {
            handleIntentExtras(getIntent());
        } else {
            mTitle = savedInstanceState.getString(Intents.EXTRA_TITLE);
            getSupportActionBar().setTitle(mTitle);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Intents.EXTRA_TITLE, mTitle);
    }

    private void handleIntentExtras(Intent intent) {
        if (intent.hasExtra(Intents.EXTRA_TITLE)) {
            mTitle = intent.getStringExtra(Intents.EXTRA_TITLE);
            getSupportActionBar().setTitle(mTitle);
        }

        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(intent.getExtras());
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.details_frag, fragment).commit();
    }
}
