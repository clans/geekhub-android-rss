package org.geekhub.example.geekhub_android_rss.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import org.geekhub.example.geekhub_android_rss.BaseActivity;
import org.geekhub.example.geekhub_android_rss.R;
import org.geekhub.example.geekhub_android_rss.fragments.LikedArticlesFragment;

public class LikedArticlesActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liked_articles_activity);

        getSupportActionBar().setTitle("Liked Articles");

        if (savedInstanceState == null) {
            handleIntentExtras(getIntent());
        }
    }

    private void handleIntentExtras(Intent intent) {
        LikedArticlesFragment fragment = new LikedArticlesFragment();
        fragment.setArguments(intent.getExtras());
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frag, fragment).commit();
    }
}
