package org.geekhub.example.geekhub_android_rss.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import org.geekhub.example.geekhub_android_rss.BaseActivity;
import org.geekhub.example.geekhub_android_rss.Constants;
import org.geekhub.example.geekhub_android_rss.R;
import org.geekhub.example.geekhub_android_rss.fragments.SettingsFragment;

public class SettingsActivity extends BaseActivity {

    private static final String OAUTH_VERIFIER = "oauth_verifier";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        if (savedInstanceState == null) {
            handleIntentExtras(getIntent());
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Uri data = intent.getData();
        if (data != null && data.toString().startsWith(Constants.CALLBACK_URL)) {
            FragmentManager fm = getSupportFragmentManager();
            SettingsFragment fragment = (SettingsFragment) fm.findFragmentById(R.id.settings_frag);

            String verifier = data.getQueryParameter(OAUTH_VERIFIER);
            if (fragment != null) {
                fragment.finishTwitterConnection(verifier);
            }
        }
    }

    private void handleIntentExtras(Intent intent) {
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(intent.getExtras());
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.settings_frag, fragment).commit();
    }
}
