package org.geekhub.example.geekhub_android_rss.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import org.geekhub.example.geekhub_android_rss.BaseFragment;
import org.geekhub.example.geekhub_android_rss.Constants;
import org.geekhub.example.geekhub_android_rss.R;
import org.geekhub.example.geekhub_android_rss.objects.Article;
import org.geekhub.example.geekhub_android_rss.objects.ArticleCollection;
import org.geekhub.example.geekhub_android_rss.util.ConnectionHelper;
import org.geekhub.example.geekhub_android_rss.util.Intents;
import org.geekhub.example.geekhub_android_rss.util.PreferenceHelper;
import com.facebook.Session;
import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.TextView;
import org.json.JSONException;

import java.io.IOException;

public class TitlesFragment extends BaseFragment {

    private View view;
    private ArticleCollection articles;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.titles_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.menu_likes).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_likes:
                startActivity(Intents.getLikedArticlesIntent(getSupportActivity()));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    articles = ConnectionHelper.getArticles();

                    updateUi();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void updateUi() {
        final ListView list = (ListView) view.findViewById(R.id.list);

        getSupportActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArticlesAdapter adapter = new ArticlesAdapter(articles);

                if (list.getAdapter() == null) {
                    list.setAdapter(adapter);
                } else {
                    adapter.notifyDataSetChanged();
                }
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Article article = (Article) adapterView.getItemAtPosition(position);
                startActivity(Intents.getDetailsIntent(
                        getSupportActivity(), article.getId(), article.getTitle(), article.getContent()));
            }
        });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                final Article article = (Article) adapterView.getItemAtPosition(position);

                CharSequence[] items = {"Share on Twitter", "Share on Facebook"};
                AlertDialog.Builder builder = new AlertDialog.Builder(getSupportActivity());
                builder.setTitle(article.getTitle())
                        .setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                switch (which) {
                                    case 0:
                                        if (PreferenceHelper.isTwitterConnected()) {
                                            startActivity(Intents.getSharingIntent(
                                                    getSupportActivity(), article.getTitle(), Constants.TWITTER));
                                        } else {
                                            // TODO add callback to fire the sharing activity after successful oauth
                                            startActivity(Intents.getSettingsIntent(getSupportActivity()));
                                        }
                                        break;
                                    case 1:
                                        Session session = Session.getActiveSession();
                                        if (session.isOpened()) {
                                            startActivity(Intents.getSharingIntent(getSupportActivity(), article.getTitle(), Constants.FACEBOOK));
                                        } else {
                                            // TODO add callback to fire the sharing activity after successful oauth
                                            startActivity(Intents.getSettingsIntent(getSupportActivity()));
                                        }
                                        break;
                                }
                            }
                        }).show();

                return true;
            }
        });
    }

    private class ArticlesAdapter extends BaseAdapter {

        private ArticleCollection articles;

        private ArticlesAdapter(ArticleCollection articles) {
            this.articles = articles;
        }

        @Override
        public int getCount() {
            return articles.asVector().size();
        }

        @Override
        public Object getItem(int i) {
            return articles.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(getSupportActivity()).inflate(R.layout.list_item);
            }

            Article article = (Article) getItem(position);

            TextView title = (TextView) view.findViewById(R.id.text);
            title.setText(article.getTitle());

            return view;
        }
    }
}
