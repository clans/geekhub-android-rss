package org.geekhub.example.geekhub_android_rss.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import org.geekhub.example.geekhub_android_rss.BaseFragment;
import org.geekhub.example.geekhub_android_rss.Constants;
import org.geekhub.example.geekhub_android_rss.R;
import org.geekhub.example.geekhub_android_rss.util.Intents;
import org.geekhub.example.geekhub_android_rss.util.PreferenceHelper;
import com.facebook.*;
import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.ProgressDialog;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

public class SharingFragment extends BaseFragment {

    private View mRootView;
    private String mSocial;
    private String mMessage;
    private ProgressDialog mDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            mSocial = args.getString(Intents.EXTRA_SOCIAL);
            mMessage = args.getString(Intents.EXTRA_MESSAGE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.sharing_fragment, container, false);

        String social = getString(mSocial.equals(Constants.TWITTER) ? R.string.twitter : R.string.facebook);
        String formatedTitle = String.format(getString(R.string.share_on_fmt), social);
        getSupportActivity().getSupportActionBar().setTitle(formatedTitle);

        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final TextView counter = (TextView) mRootView.findViewById(R.id.counter);
        final TextView warning = (TextView) mRootView.findViewById(R.id.warning);

        EditText msgInput = (EditText) mRootView.findViewById(R.id.message);
        msgInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = editable.length();
                counter.setText(String.valueOf(length));

                int max;
                String social;
                if (mSocial.equals(Constants.TWITTER)) {
                    max = Constants.TWITTER_MAX;
                    social = getString(R.string.twitter);
                } else {
                    max = Constants.FACEBOOK_MAX;
                    social = getString(R.string.facebook);
                }

                String warningMsg = String.format(getString(R.string.warning_message_fmt), social);

                if (length > max) {
                    counter.setTextColor(getActivity().getResources().getColor(R.color.holo_red_light));
                    warning.setText(warningMsg);
                    warning.setVisibility(View.VISIBLE);
                } else {
                    counter.setTextColor(R.color.primary_text_holo_light);
                    warning.setVisibility(View.GONE);
                }
            }
        });

        if (mMessage != null) {
            msgInput.setText(mMessage);
        }

        Button cancel = (Button) mRootView.findViewById(R.id.cancel);
        Button send = (Button) mRootView.findViewById(R.id.send);

        cancel.setOnClickListener(listener);
        send.setOnClickListener(listener);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            EditText msgInput = (EditText) mRootView.findViewById(R.id.message);
            String message = msgInput.getText().toString().trim();

            switch (view.getId()) {
                case R.id.cancel:
                    getActivity().onBackPressed();
                    break;
                case R.id.send:
                    if (mSocial.equals(Constants.TWITTER)) {
                        if (TextUtils.isEmpty(message)) {
                            getSupportActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getSupportActivity(), "Error: message is empty", Toast.LENGTH_SHORT).show();
                                }
                            });

                            break;
                        }

                        new ShareOnTwitter().execute(message);
                    } else if (mSocial.equals(Constants.FACEBOOK)) {
                        shareOnFacebook(message);
                    }
                    break;
            }
        }
    };

    private class ShareOnTwitter extends AsyncTask<String, Void, TwitterException> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = new ProgressDialog(getSupportActivity());
            mDialog.setMessage(getString(R.string.sending_to_twitter));
            mDialog.setIndeterminate(false);
            mDialog.setCancelable(false);
            mDialog.show();
        }

        /**
         * Updating Twitter status
         */
        protected TwitterException doInBackground(String... args) {
            String message = args[0];
            try {
                if (message.length() > Constants.TWITTER_MAX) {
                    message = message.substring(0, Constants.TWITTER_MAX);
                }

                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setOAuthConsumerKey(Constants.CONSUMER_KEY);
                builder.setOAuthConsumerSecret(Constants.CONSUMER_SECRET);

                // Access Token
                String access_token = PreferenceHelper.getTwitterToken();
                // Access Token Secret
                String access_token_secret = PreferenceHelper.getTwitterTokenSecret();

                AccessToken accessToken = new AccessToken(access_token, access_token_secret);
                Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

                // Update status
                twitter4j.Status response = twitter.updateStatus(message);

                Log.d("Status", response.getText());
            } catch (TwitterException e) {
                // Error in updating status
                Log.w("Twitter Update Error", e.getMessage());
                return e;
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog and show the data in UI Thread
         */
        protected void onPostExecute(final TwitterException e) {
            // dismiss the dialog after getting all products
            mDialog.dismiss();
            // updating UI from Background Thread
            getSupportActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getSupportActivity(), e == null ? getString(R.string.twitter_successfully)
                            : e.getErrorMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void shareOnFacebook(String message) {
        Session session = Session.getActiveSession();

        if (session != null) {
            Bundle postParams = new Bundle();
            postParams.putString("message", message);

            Request.Callback callback= new Request.Callback() {
                public void onCompleted(Response response) {
                    FacebookRequestError error = response.getError();
                    if (error != null) {
                        Toast.makeText(getSupportActivity(), error.getErrorMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getSupportActivity(), getString(R.string.facebook_successfully), Toast.LENGTH_SHORT).show();
                    }
                }
            };

            Request request = new Request(session, "me/feed", postParams, HttpMethod.POST, callback);

            RequestAsyncTask task = new RequestAsyncTask(request);
            task.execute();
        }
    }


}
