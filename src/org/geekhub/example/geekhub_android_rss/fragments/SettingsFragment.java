package org.geekhub.example.geekhub_android_rss.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import org.geekhub.example.geekhub_android_rss.BaseFragment;
import org.geekhub.example.geekhub_android_rss.Constants;
import org.geekhub.example.geekhub_android_rss.R;
import org.geekhub.example.geekhub_android_rss.util.PreferenceHelper;
import com.facebook.*;
import com.facebook.model.GraphUser;
import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import java.util.Arrays;

public class SettingsFragment extends BaseFragment {

    private View mRootView;
    private Twitter mTwitter;
    private RequestToken mRequestToken;
    private Session.StatusCallback mStatusCallback = new SessionStatusCallback();
    private GraphUser mUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.settings_fragment, container, false);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        Session session = Session.getActiveSession();
        if (session == null) {
            if (savedInstanceState != null) {
                session = Session.restoreSession(getSupportActivity(), null, mStatusCallback, savedInstanceState);
            }
            if (session == null) {
                session = new Session.Builder(getSupportActivity()).setApplicationId(getSupportActivity().getString(R.string.app_id)).build();
            }
            Session.setActiveSession(session);
            if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
                session.openForRead(new Session.OpenRequest(this).setCallback(mStatusCallback));
            }
        }

        updateConnectionsState();
        makeMeRequest(session);

        mRootView.findViewById(R.id.twitter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PreferenceHelper.isTwitterConnected()) {
                    new AlertDialog.Builder(getSupportActivity())
                            .setTitle(R.string.confirm)
                            .setMessage(R.string.remove_connection)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    removeTwitterConnection();
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();
                } else {
                    addTwitterConnection();
                }
            }
        });

        final Session finalSession = session;
        mRootView.findViewById(R.id.facebook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (finalSession.isOpened()) {
                    new AlertDialog.Builder(getSupportActivity())
                            .setTitle(R.string.confirm)
                            .setMessage(R.string.remove_connection)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    removeFacebookConnection();
                                }
                            })
                            .setNegativeButton("No", null)
                            .show();
                } else {
                    addFacebookConnection();
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Session session = Session.getActiveSession();
        Session.saveSession(session, outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.menu_settings).setVisible(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Session.getActiveSession().addCallback(mStatusCallback);
    }

    @Override
    public void onStop() {
        super.onStop();
        Session.getActiveSession().removeCallback(mStatusCallback);
    }

    private void addTwitterConnection() {
        new Thread(new Runnable() {
            public void run() {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setOAuthConsumerKey(Constants.CONSUMER_KEY);
                builder.setOAuthConsumerSecret(Constants.CONSUMER_SECRET);

                Configuration configuration = builder.build();
                mTwitter = new TwitterFactory(configuration).getInstance();

                try {
                    mRequestToken = mTwitter.getOAuthRequestToken(Constants.CALLBACK_URL);
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mRequestToken.getAuthenticationURL())));
                } catch (TwitterException e) {
                    Log.e(Constants.LOG_TAG, e.getErrorMessage(), e);
                    Toast.makeText(getSupportActivity(), e.getErrorMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }).start();
    }

    public void finishTwitterConnection(final String verifier) {
        new Thread(new Runnable() {
            public void run() {
                if (verifier != null) {
                    try {
                        AccessToken accessToken = mTwitter.getOAuthAccessToken(mRequestToken, verifier);
                        long userID = accessToken.getUserId();
                        User user = mTwitter.showUser(userID);
                        String username = user.getScreenName();

                        PreferenceHelper.setTwitterConncetion(username, accessToken.getToken(), accessToken.getTokenSecret());
                        updateConnectionsState();
                    } catch (TwitterException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void removeTwitterConnection() {
        PreferenceHelper.removeTwitterConnection();
        updateConnectionsState();
    }

    private void addFacebookConnection() {
        Session session = Session.getActiveSession();
        if (!session.isOpened() && !session.isClosed()) {
            session.openForPublish(new Session.OpenRequest(this).setCallback(mStatusCallback).setPermissions(Arrays.asList("publish_actions")));
        } else {
            Session.openActiveSession(getSupportActivity(), this, true, mStatusCallback);
        }
    }

    private void removeFacebookConnection() {
        Session session = Session.getActiveSession();
        if (!session.isClosed()) {
            session.closeAndClearTokenInformation();
        }
    }

    public void updateConnectionsState() {
        final String twUsername = PreferenceHelper.getTwitterUsername();
        final Session session = Session.getActiveSession();

        getSupportActivity().runOnUiThread(new Runnable() {
            public void run() {
                TextView twUser = (TextView) mRootView.findViewById(R.id.tw_user);
                TextView fbUser = (TextView) mRootView.findViewById(R.id.fb_user);

                if (twUsername != null) {
                    twUser.setText("@" + twUsername);
                } else {
                    twUser.setText(R.string.connect);
                }

                if (session.isOpened() && mUser != null) {
                    fbUser.setText(mUser.getFirstName() + " " + mUser.getLastName());
                } else {
                    fbUser.setText(R.string.connect);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
    }

    private void makeMeRequest(final Session session) {
        Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser user, Response response) {
                // If the response is successful
                if (session == Session.getActiveSession()) {
                    if (user != null) {
                        mUser = user;
                    }
                }

                updateConnectionsState();

                if (response.getError() != null) {
                    // Handle errors, will do so later.
                    Log.d(Constants.LOG_TAG, response.getError().getErrorMessage(), response.getError().getException());
                }
            }
        });
    }

    private class SessionStatusCallback implements Session.StatusCallback {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            if (state.isOpened()) {
                makeMeRequest(session);
            } else {
                updateConnectionsState();
            }
        }

    }
}
