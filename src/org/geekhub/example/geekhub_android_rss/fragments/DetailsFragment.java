package org.geekhub.example.geekhub_android_rss.fragments;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import org.geekhub.example.geekhub_android_rss.BaseFragment;
import org.geekhub.example.geekhub_android_rss.Constants;
import org.geekhub.example.geekhub_android_rss.R;
import org.geekhub.example.geekhub_android_rss.database.ArticlesContentProvider;
import org.geekhub.example.geekhub_android_rss.database.ArticlesTable;
import org.geekhub.example.geekhub_android_rss.util.Intents;
import org.geekhub.example.geekhub_android_rss.util.PreferenceHelper;
import com.facebook.Session;
import org.holoeverywhere.LayoutInflater;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class DetailsFragment extends BaseFragment {

    private View mRootView;
    private long mId;
    private String mTitle;
    private String mContent;
    private Menu mOptionsMenu;
    private boolean isLiked;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            mId = args.getLong(Intents.EXTRA_ID);
            mTitle = args.getString(Intents.EXTRA_TITLE);
            mContent = args.getString(Intents.EXTRA_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.details_fragment, container, false);
        return mRootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        updateUi();
        loadFromDb();
    }

    private void loadFromDb() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                setLikeActionButtonProgress(true);

                Uri uri = Uri.parse(ArticlesContentProvider.CONTENT_URI + "/" + mId);
                Cursor cursor = getSupportActivity().getContentResolver()
                        .query(uri, ArticlesTable.PROJECTION, null, null, null);

                if (cursor != null) {
                    if (cursor.moveToNext()) {
                        int colId = cursor.getColumnIndex(ArticlesTable.COLUMN_ID);
                        long id = cursor.getLong(colId);

                        isLiked = id == mId;
                        setLikeActionButtonState(isLiked);
                    } else {
                        setLikeActionButtonState(false);
                    }

                    cursor.close();
                }

                setLikeActionButtonProgress(false);
            }
        }).start();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mOptionsMenu = menu;
        menu.findItem(R.id.menu_like).setVisible(true);
        menu.findItem(R.id.menu_share).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_like:
                setLikeActionButtonProgress(true);
                articleAddOrRemove(isLiked);
                break;
            case R.id.twitter:
                if (PreferenceHelper.isTwitterConnected()) {
                    startActivity(Intents.getSharingIntent(getSupportActivity(), mTitle, Constants.TWITTER));
                } else {
                    // TODO add callback to fire the sharing activity after successful oauth
                    startActivity(Intents.getSettingsIntent(getSupportActivity()));
                }
                break;
            case R.id.facebook:
                Session session = Session.getActiveSession();
                if (session.isOpened()) {
                    startActivity(Intents.getSharingIntent(getSupportActivity(), mTitle, Constants.FACEBOOK));
                } else {
                    // TODO add callback to fire the sharing activity after successful oauth
                    startActivity(Intents.getSettingsIntent(getSupportActivity()));
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void articleAddOrRemove(final boolean liked) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (liked) {
                    Uri uri = Uri.parse(ArticlesContentProvider.CONTENT_URI + "/" + mId);
                    getSupportActivity().getContentResolver().delete(uri,
                            ArticlesTable.COLUMN_ID + "= ?", new String[]{String.valueOf(mId)});
                } else {
                    ContentValues values = new ContentValues();
                    values.put(ArticlesTable.COLUMN_ID, mId);
                    values.put(ArticlesTable.COLUMN_TITLE, mTitle);
                    values.put(ArticlesTable.COLUMN_CONTENT, mContent);

                    getSupportActivity().getContentResolver().insert(ArticlesContentProvider.CONTENT_URI, values);
                }

                loadFromDb();
            }
        }).start();
    }

    private void updateUi() {
        getSupportActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    WebView webView = (WebView) mRootView.findViewById(R.id.webView);
                    webView.getSettings().setBuiltInZoomControls(true);
                    webView.loadData(URLEncoder.encode(mContent, "UTF-8").replaceAll("\\+", "%20"), "text/html", "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setLikeActionButtonState(final boolean isLiked) {
        if (mOptionsMenu == null) return;

        this.isLiked = isLiked;

        getSupportActivity().runOnUiThread(new Runnable() {
            public void run() {
                MenuItem likeItem = mOptionsMenu.findItem(R.id.menu_like);
                if (likeItem != null) {
                    likeItem.setIcon(isLiked ? R.drawable.ic_menu_liked : R.drawable.ic_menu_not_liked);
                }
            }
        });
    }

    private void setLikeActionButtonProgress(final boolean loading) {
        if (mOptionsMenu == null) return;

        getSupportActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MenuItem likeItem = mOptionsMenu.findItem(R.id.menu_like);
                if (likeItem != null) {
                    if (loading) {
                        likeItem.setActionView(R.layout.actionbar_indeterminate_progress);
                    } else {
                        likeItem.setActionView(null);
                    }
                }
            }
        });
    }
}
