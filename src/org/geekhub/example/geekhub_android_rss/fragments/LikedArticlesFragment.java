package org.geekhub.example.geekhub_android_rss.fragments;

import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import org.geekhub.example.geekhub_android_rss.BaseFragment;
import org.geekhub.example.geekhub_android_rss.Constants;
import org.geekhub.example.geekhub_android_rss.R;
import org.geekhub.example.geekhub_android_rss.database.ArticlesContentProvider;
import org.geekhub.example.geekhub_android_rss.database.ArticlesTable;
import org.geekhub.example.geekhub_android_rss.util.Intents;
import org.geekhub.example.geekhub_android_rss.util.PreferenceHelper;
import com.facebook.Session;
import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.Toast;

public class LikedArticlesFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private View view;
    private SimpleCursorAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.titles_fragment, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter = new SimpleCursorAdapter(getActivity(), R.layout.list_item, null,
                new String[]{ArticlesTable.COLUMN_TITLE}, new int[]{R.id.text}, 0);

        ListView list = (ListView) view.findViewById(R.id.list);
        list.setAdapter(mAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Cursor cursor = (Cursor) adapterView.getItemAtPosition(position);
                int colId = cursor.getColumnIndex(ArticlesTable.COLUMN_ID);
                int colTitle = cursor.getColumnIndex(ArticlesTable.COLUMN_TITLE);
                int colContent = cursor.getColumnIndex(ArticlesTable.COLUMN_CONTENT);

                startActivity(Intents.getDetailsIntent(getSupportActivity(),
                        cursor.getLong(colId), cursor.getString(colTitle), cursor.getString(colContent)));
            }
        });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                final Cursor cursor = (Cursor) adapterView.getItemAtPosition(position);
                int colTitle = cursor.getColumnIndex(ArticlesTable.COLUMN_TITLE);

                CharSequence[] items = {"Share on Twitter", "Share on Facebook", "Remove from database"};

                AlertDialog.Builder builder = new AlertDialog.Builder(getSupportActivity());
                builder.setTitle(cursor.getString(colTitle))
                        .setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                int colId = cursor.getColumnIndex(ArticlesTable.COLUMN_ID);
                                int colTitle = cursor.getColumnIndex(ArticlesTable.COLUMN_TITLE);

                                switch (which) {
                                    case 0:
                                        if (PreferenceHelper.isTwitterConnected()) {
                                            startActivity(Intents.getSharingIntent(
                                                    getSupportActivity(), cursor.getString(colTitle), Constants.TWITTER));
                                        } else {
                                            // TODO add callback to fire the sharing activity after successful oauth
                                            startActivity(Intents.getSettingsIntent(getSupportActivity()));
                                        }
                                        break;
                                    case 1:
                                        Session session = Session.getActiveSession();
                                        if (session.isOpened()) {
                                            startActivity(Intents.getSharingIntent(getSupportActivity(), cursor.getString(colTitle), Constants.FACEBOOK));
                                        } else {
                                            // TODO add callback to fire the sharing activity after successful oauth
                                            startActivity(Intents.getSettingsIntent(getSupportActivity()));
                                        }
                                        break;
                                    case 2:
                                        removeFromDb(cursor.getLong(colId));
                                        break;
                                }
                            }
                        }).show();

                return true;
            }
        });

        // Prepare the loader. Either re-connect with an existing one, or start a new one.
        getLoaderManager().initLoader(0, null, this);
    }

    private void removeFromDb(final long id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Uri uri = Uri.parse(ArticlesContentProvider.CONTENT_URI + "/" + id);
                int count = getSupportActivity().getContentResolver().delete(uri,
                        ArticlesTable.COLUMN_ID + "= ?", new String[]{String.valueOf(id)});

                if (count > 0) {
                    getSupportActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getSupportActivity(),
                                    "Article was removed successfully", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(getSupportActivity(), ArticlesContentProvider.CONTENT_URI,
                ArticlesTable.PROJECTION, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        mAdapter.swapCursor(null);
    }
}
