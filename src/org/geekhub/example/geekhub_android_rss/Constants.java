package org.geekhub.example.geekhub_android_rss;

public final class Constants {

    private Constants() {
    }

    public static final String CONSUMER_KEY = "1bgMzWuDjHhXMmL3kUIYUQ";
    public static final String CONSUMER_SECRET = "D3nqS2GFRF3EBAK2EHAJNnlrCeBXKFiNmqJ8A2OE4mw";
    public static final String REQUEST_URL = "https://api.twitter.com/oauth/request_token";
    public static final String ACCESS_URL = "https://api.twitter.com/oauth/access_token";
    public static final String AUTHORIZE_URL = "https://api.twitter.com/oauth/authorize";
    public static final String CALLBACK_URL = "callback://geekhub";

    public static final String LOG_TAG = "GeekHub";

    public static final String TWITTER = "twitter";
    public static final String FACEBOOK = "facebook";
    public static final int TWITTER_MAX = 140;
    public static final int FACEBOOK_MAX = 420;
}
