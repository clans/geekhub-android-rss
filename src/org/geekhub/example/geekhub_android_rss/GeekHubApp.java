package org.geekhub.example.geekhub_android_rss;

import org.geekhub.example.geekhub_android_rss.util.PreferenceHelper;
import org.holoeverywhere.app.Application;

public class GeekHubApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        PreferenceHelper.init(this);
    }
}
